The codebase for ModelGuard has been moved. It can now be found [here](https://gitlab.com/modelguard/modelguard).

The case studies for ModelGuard can now be found [here](https://gitlab.com/modelguard/case-studies).
